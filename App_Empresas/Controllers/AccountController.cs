﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace App_Empresas.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/users/auth/sign_in")]
    public class AccountController : Controller
    {
        private IConfiguration _config;

        public AccountController(IConfiguration config)
        {
            _config = config;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken([FromBody]LoginModel login)
        {
            var user = Authenticate(login);
            IActionResult response = Unauthorized(new { success = false, errors = "Invalid login credentials. Please try again." });

            if (user != null)
            {
                var tokenString = BuildToken(user);

                Response.Headers["token-type"] = "Bearer";
                Response.Headers["uid"] = user.Email;
                Response.Headers["client"] = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                Response.Headers["access-token"] = tokenString;
                Response.Headers["expiry"] = DateTime.Now.AddMinutes(60).ToString();

                response = Ok(new { success = true });
            }

            return response;
        }

        private string BuildToken(UserModel user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: DateTime.Now.AddMinutes(60),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private UserModel Authenticate(LoginModel login)
        {
            UserModel user = null;

            if (login.Username == "mario" && login.Password == "secret")
            {
                user = new UserModel { Name = "Mario Rossi", Email = "mario.rossi@domain.com" };
            }
            return user;
        }

        public class LoginModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        private class UserModel
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime Birthdate { get; set; }
        }
    }
}
