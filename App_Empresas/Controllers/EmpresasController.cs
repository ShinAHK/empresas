﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using App_Empresas.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;

namespace App_Empresas.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/empresas")]
    public class EmpresasController : ControllerBase
    {
        private readonly EmpresasContext _context;

        public EmpresasController(EmpresasContext context)
        {
            _context = context;
        }

        // GET: api/Todo
        //[Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmpresasEnterprises>>> GetEmpresasEnterprises()
        {
            return await _context.Enterprises.ToListAsync();
        }

        // GET: api/Todo/5
        //[Authorize]
        [HttpGet("{Id}")]
        public async Task<ActionResult<EmpresasEnterprises>> GetEmpresasEnterprise(int Id)
        {
            var enterprise = await _context.Enterprises.FindAsync(Id);

            if (enterprise == null)
            {
                return NotFound();
            }

            return enterprise;
        }

        public async Task<ActionResult<EmpresasEnterprises>> Get(int enterprise_types, string name)
        {
            if (enterprise_types <= 0)
                return BadRequest("Enterprise type is invalid");
            if (string.IsNullOrEmpty(name))
                return BadRequest("Name is empty or invalid");

            var empresa = _context.Enterprises.Where(e => e.Enterprise_name.StartsWith(name) && e.Enterprise_type_Id == enterprise_types).OrderBy(e => e.Id);

            if (empresa == null)
                return NotFound();

            return Ok(empresa);
        }

    }
}
