﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App_Empresas.Models
{
    public class EmpresasEnterprises
    {
        public int Id { get; set; }
        public string Email_enterprise { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        public bool Own_enterprise { get; set; }
        public string Enterprise_name { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Value { get; set; }
        public decimal Share_price { get; set; }

        [JsonIgnore]
        public int Enterprise_type_Id { get; set; }

        [ForeignKey(nameof(Enterprise_type_Id))]
        public EmpresasEnterprise_Type Enterprises_Enterprise_Types { get; set; }
    }
}
