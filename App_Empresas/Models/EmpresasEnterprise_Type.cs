﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Empresas.Models
{
    public class EmpresasEnterprise_Type
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("enterprise_type_name")]
        public string Enterprise_type_name { get; set; }
    }
}
