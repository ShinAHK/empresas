﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Empresas.Models
{
    public class EmpresasContext : DbContext
    {
        public EmpresasContext(DbContextOptions<EmpresasContext> options) : base(options)
        {
        }

        public DbSet<EmpresasEnterprises> Enterprises { get; set; }
        public DbSet<EmpresasEnterprise_Type> Enterprise_types { get; set; }
        //public DbSet<EmpresasUsers> Users { get; set; }
    }
}
